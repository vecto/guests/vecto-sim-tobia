# Simulation Models

In this chapter the used component models for the simulation are described.

* [Powertrain and Components Structure](#powertrain-and-components-structure)
* [Driver: Acceleration Limiting](#driver-acceleration-limiting)
* [Driver: Look-Ahead Coasting](#driver-look-ahead-coasting)
* [ADAS: Overspeed](#driver-overspeed)
* [ADAS Technologies](#vehicle-adas-technologies)
* [Vehicle: Cross Wind Correction](#vehicle-cross-wind-correction)
* [Vehicle: Rolling Resistance Coefficient](#vehicle-rolling-resistance-coefficient)
* [Engine: Fuel Consumption Calculation](#engine-fuel-consumption-calculation)
* [Engine: Transient Full Load](#engine-transient-full-load)
* [Engine: WHTC Correction Factors](#engine-correction-factors)
* [Fuel properties](#fuel-properties)
* [Torque and Speed Limitations](#torque-and-speed-limitations)
* [Gearbox: Gear Shift Model](#gearbox-gear-shift-model)
* [Gearbox: MT and AMT Gearshift Rules](#gearbox-mt-and-amt-gearshift-rules)
* [Gearbox: AT Gearshift Rules](#gearbox-at-gearshift-rules)
* [Torque Converter Model](#torque-converter-model)
* [Auxiliaries](#auxiliaries)
* [Engine Only Mode](#engine-only-mode)
* [Pwheel-Input (SiCo Mode)](#pwheel-input-sico-mode)
