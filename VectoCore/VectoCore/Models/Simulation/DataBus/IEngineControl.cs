﻿namespace TUGraz.VectoCore.Models.Simulation.DataBus
{
	public interface IEngineControl
	{
		bool CombustionEngineOn { get; set; }
	}
}