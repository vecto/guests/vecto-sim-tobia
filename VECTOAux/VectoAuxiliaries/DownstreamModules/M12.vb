﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.

Imports TUGraz.VectoCommon.Utils
Imports VectoAuxiliaries.Electrics
Imports VectoAuxiliaries.Pneumatics
Imports VectoAuxiliaries.Hvac

Namespace DownstreamModules
	Public Class M12
		Implements IM12

		Private Class Point
			Public X As Joule
			Public Y As Kilogram
		End Class

		Private M11 As IM11
		Private M10 As IM10
		Private Signals As ISignals
		Private _P1X As Joule
		Private _P1Y As Kilogram
		Private _P2X As Joule
		Private _P2Y As Kilogram
		Private _P3X As Joule
		Private _P3Y As Kilogram
		Private _XT As Joule
		Private _INTERP1 As Kilogram
		Private _INTERP2 As Kilogram

		Private Sub setPoints()

			_P1X = 0.SI(Of Joule)()
			_P1Y = M11.TotalCycleFuelConsumptionZeroElectricalLoad
			_P2X = M11.SmartElectricalTotalCycleEletricalEnergyGenerated
			_P2Y = M11.TotalCycleFuelConsumptionSmartElectricalLoad
			_P3X = M11.StopStartSensitiveTotalCycleElectricalDemand
			_P3Y = M10.AverageLoadsFuelConsumptionInterpolatedForPneumatics
			_XT = M11.TotalCycleElectricalDemand
		End Sub

		'Interpolation 
		Private Function Sum1() As Kilogram

			Dim P1 As Point = New Point With {.X = 0.SI(Of Joule)(), .Y = M11.TotalCycleFuelConsumptionZeroElectricalLoad}
			Dim P2 As Point = New Point _
					With {.X = M11.SmartElectricalTotalCycleEletricalEnergyGenerated * Signals.StoredEnergyEfficiency,
					.Y = M11.TotalCycleFuelConsumptionSmartElectricalLoad}

			Dim IP5x As Joule = M11.TotalCycleElectricalDemand
			

			Dim TanTeta As double = ((P2.Y - P1.Y).Value() / (P2.X - P1.X).Value())

		    Dim IP5y As Kilogram = (P1.Y.Value() + (TanTeta * IP5x.Value())).SI(of Kilogram)

			_INTERP1 = IP5y

			setPoints()

			Return If(Double.IsNaN(IP5y.Value()), 0.SI(Of Kilogram), IP5y)
		End Function

		Private Function Sum2() As Kilogram

			Dim P1 As Point = New Point With {.X = 0.SI(Of Joule)(), .Y = M11.TotalCycleFuelConsumptionZeroElectricalLoad}
			Dim P3 As Point = New Point _
					With {.X = M11.StopStartSensitiveTotalCycleElectricalDemand,
					.Y = M10.AverageLoadsFuelConsumptionInterpolatedForPneumatics}

			Dim IP5x As Joule = M11.TotalCycleElectricalDemand
			Dim IP5y As Kilogram

			Dim TanTeta As Double = (P3.Y - P1.Y).Value() / (P3.X - P1.X).Value()

			IP5y = P1.Y + (TanTeta * IP5x.Value()).SI(Of Kilogram)()

			_INTERP2 = IP5y

			Return If(Double.IsNaN(IP5y.Value()), 0.SI(Of Kilogram), IP5y)
		End Function

		'Constructor
		Public Sub New(m10 As IM10, m11 As IM11, signals As ISignals)

			Me.M10 = m10
			Me.M11 = m11
			Me.Signals = signals
		End Sub

		'Main Class Outputs
		Public ReadOnly Property FuelconsumptionwithsmartElectricsandAveragePneumaticPowerDemand As Kilogram _
			Implements IM12.FuelconsumptionwithsmartElectricsandAveragePneumaticPowerDemand
			Get

				'SCHM 3_2
				Dim interp1 As Kilogram = Sum1()

				interp1 =
					If _
						(Not Double.IsNaN(interp1.Value()) AndAlso M11.StopStartSensitiveTotalCycleElectricalDemand > 0, interp1,
						M11.TotalCycleFuelConsumptionZeroElectricalLoad)
				Return interp1
			End Get
		End Property

		Public ReadOnly Property BaseFuelConsumptionWithTrueAuxiliaryLoads As Kilogram _
			Implements IM12.BaseFuelConsumptionWithTrueAuxiliaryLoads
			Get

				'SCM 3_02
				Dim interp2 As Kilogram = Sum2()

				interp2 =
					If _
						(Not Double.IsNaN(interp2.Value()) AndAlso M11.StopStartSensitiveTotalCycleElectricalDemand > 0, interp2,
						M10.AverageLoadsFuelConsumptionInterpolatedForPneumatics)
				Return interp2
			End Get
		End Property

		Public ReadOnly Property StopStartCorrection As Double Implements IM12.StopStartCorrection
			Get

				Dim _stopStartCorrection As Scalar = BaseFuelConsumptionWithTrueAuxiliaryLoads() /
													If _
														(M10.AverageLoadsFuelConsumptionInterpolatedForPneumatics > 0,
														M10.AverageLoadsFuelConsumptionInterpolatedForPneumatics, 1.SI(Of Kilogram))

				Return If(_stopStartCorrection > 0, _stopStartCorrection.Value(), 1)
			End Get
		End Property

		'Diagnostics Signal Exposure only. Does not materially affect class operation.
		Public ReadOnly Property INTRP1 As Kilogram Implements IM12.INTRP1
			Get
				Return _INTERP1
			End Get
		End Property

		Public ReadOnly Property INTRP2 As Kilogram Implements IM12.INTRP2
			Get
				Return _INTERP2
			End Get
		End Property

		Public ReadOnly Property P1X As Joule Implements IM12.P1X
			Get
				Return _P1X
			End Get
		End Property

		Public ReadOnly Property P1Y As Kilogram Implements IM12.P1Y
			Get
				Return _P1Y
			End Get
		End Property

		Public ReadOnly Property P2X As Joule Implements IM12.P2X
			Get
				Return _P2X
			End Get
		End Property

		Public ReadOnly Property P2Y As Kilogram Implements IM12.P2Y
			Get
				Return _P2Y
			End Get
		End Property

		Public ReadOnly Property P3X As Joule Implements IM12.P3X
			Get
				Return _P3X
			End Get
		End Property

		Public ReadOnly Property P3Y As Kilogram Implements IM12.P3Y
			Get
				Return _P3Y
			End Get
		End Property

		Public ReadOnly Property XTAIN As Joule Implements IM12.XTAIN
			Get
				Return _XT
			End Get
		End Property
	End Class
End Namespace


