﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.

Imports TUGraz.VectoCommon.Utils
Imports VectoAuxiliaries.Pneumatics

Namespace DownstreamModules
	Public Class M10
		Implements IM10

		'Aggregators
		Private _AverageAirConsumedLitre As NormLiter

		'Diagnostics
		Private x1 As NormLiter
		Private y1 As Kilogram
		Private x2 As NormLiter
		Private y2 As Kilogram
		Private x3 As NormLiter
		Private y3 As Kilogram
		Private xTA As NormLiter
		Private interp1 As Kilogram
		Private interp2 As Kilogram

		Public ReadOnly Property P1X As NormLiter Implements IM10.P1X
			Get
				Return x1
			End Get
		End Property

		Public ReadOnly Property P1Y As Kilogram Implements IM10.P1Y
			Get
				Return y1
			End Get
		End Property

		Public ReadOnly Property P2X As NormLiter Implements IM10.P2X
			Get
				Return x2
			End Get
		End Property

		Public ReadOnly Property P2Y As Kilogram Implements IM10.P2Y
			Get
				Return y2
			End Get
		End Property

		Public ReadOnly Property P3X As NormLiter Implements IM10.P3X
			Get
				Return x3
			End Get
		End Property

		Public ReadOnly Property P3Y As Kilogram Implements IM10.P3Y
			Get
				Return y3
			End Get
		End Property

		Public ReadOnly Property XTAIN As NormLiter Implements IM10.XTAIN
			Get
				Return xTA
			End Get
		End Property

		Public ReadOnly Property INTRP1 As Kilogram Implements IM10.INTRP1
			Get
				Return interp1
			End Get
		End Property

		Public ReadOnly Property INTRP2 As Kilogram Implements IM10.INTRP2
			Get
				Return interp2
			End Get
		End Property


		'Private

#Region "Private Fields  = > Constructor Requirements"

		Private m3 As IM3_AveragePneumaticLoadDemand
		Private m9 As IM9
		'Not Currently used but there for ease of refactoring in future.
		Private signals As ISignals

#End Region

		Private Enum InterpolationType
			NonSmartPneumtaics
			SmartPneumtaics
		End Enum

		Private Function Interpolate(interpType As InterpolationType) As Kilogram

			Dim returnValue As Kilogram = 0.SI(Of Kilogram)()
			' Dim x1,y1,x2,y2,x3,y3, xTA As Single

			x1 = m9.LitresOfAirCompressorOnContinually
			y1 = m9.TotalCycleFuelConsumptionCompressorOnContinuously
			x2 = 0.SI(Of NormLiter)()
			y2 = m9.TotalCycleFuelConsumptionCompressorOffContinuously
			x3 = m9.LitresOfAirCompressorOnOnlyInOverrun
			y3 = m9.TotalCycleFuelConsumptionCompressorOffContinuously


			xTA = _AverageAirConsumedLitre	 'm3.AverageAirConsumedPerSecondLitre


			Select Case interpType

				'Non-Smart Pneumatics ( OUT 1 )
				Case InterpolationType.NonSmartPneumtaics
					'returnValue = (y2 + (((y1 - y2) * xTA) / x1))
					returnValue = VectoMath.Interpolate(x1, x2, y1, y2, xTA)
					interp1 = returnValue

					'Smart Pneumatics ( OUT 2 )
				Case InterpolationType.SmartPneumtaics
					'returnValue = (y3 + (((y1 - y3) / (x1 - x3)) * (xTA - x3)))
					returnValue = VectoMath.Interpolate(x1, x3, y1, y3, xTA)
					interp2 = returnValue
			End Select


			Return returnValue
		End Function

		'Public 

#Region "Public Properties"

		Public ReadOnly Property AverageLoadsFuelConsumptionInterpolatedForPneumatics As Kilogram _
			Implements IM10.AverageLoadsFuelConsumptionInterpolatedForPneumatics
			Get

				'SCHM 3_02
				Dim intrp1 As Kilogram = Interpolate(InterpolationType.NonSmartPneumtaics)
				'intrp1 = If(Not Single.IsNaN(intrp1), intrp1, 0)
				Return intrp1
			End Get
		End Property

		Public ReadOnly Property FuelConsumptionSmartPneumaticsAndAverageElectricalPowerDemand As Kilogram _
			Implements IM10.FuelConsumptionSmartPneumaticsAndAverageElectricalPowerDemand
			Get

				'SCHM 3_02
				Dim intrp2 As Kilogram = Interpolate(InterpolationType.SmartPneumtaics)
				'intrp2 = If(Not Single.IsNaN(intrp2), intrp2, 0)
				Return intrp2
			End Get
		End Property

#End Region

#Region "Contructors"

		Public Sub New(m3 As IM3_AveragePneumaticLoadDemand, m9 As IM9, signals As ISignals)

			Me.m3 = m3
			Me.m9 = m9
			Me.signals = signals
			_AverageAirConsumedLitre = 0.SI(Of NormLiter)()
		End Sub

#End Region

		Public Sub CycleStep(stepTimeInSeconds As Second) Implements IM10.CycleStep

			_AverageAirConsumedLitre +=
				If _
					(Double.IsNaN(m3.AverageAirConsumedPerSecondLitre.Value()), 0.SI(Of NormLiter),
					m3.AverageAirConsumedPerSecondLitre * stepTimeInSeconds)
		End Sub
	End Class
End Namespace


