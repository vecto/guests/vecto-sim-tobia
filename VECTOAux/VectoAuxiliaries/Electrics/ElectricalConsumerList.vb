﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.


Imports System.Collections.Generic
Imports TUGraz.VectoCommon.Utils

Namespace Electrics
	Public Class ElectricalConsumerList
		Implements IElectricalConsumerList

		Private _items As New List(Of IElectricalConsumer)
		Private _powernetVoltage As Double
		Private _doorDutyCycleZeroToOne As Double


		'Constructor
		Public Sub New(powernetVoltage As Double, doorDutyCycle_ZeroToOne As Double,
						Optional createDefaultList As Boolean = False)

			_powernetVoltage = powernetVoltage

			If createDefaultList Then

				_items = GetDefaultConsumerList()

			End If


			_doorDutyCycleZeroToOne = doorDutyCycle_ZeroToOne
		End Sub

		'Transfers the Info comments from a default set of consumables to a live set.
		'This way makes the comments not dependent on saved data.
		Public Sub MergeInfoData() Implements IElectricalConsumerList.MergeInfoData

			If _items.Count <> GetDefaultConsumerList().Count Then Return

			Dim dflt As List(Of IElectricalConsumer) = GetDefaultConsumerList()

			For idx As Integer = 0 To _items.Count - 1

				_items(idx).Info = dflt(idx).Info

			Next
		End Sub

		'Initialise default set of consumers
		Public Function GetDefaultConsumerList() As List(Of IElectricalConsumer)

			'This populates the default settings as per engineering spreadsheet.
			'Vehicle Basic Equipment' category can be added or remove by customers.
			'At some time in the future, this may be removed and replace with file based consumer lists.

			Dim items As New List(Of IElectricalConsumer)

			Dim c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20 As IElectricalConsumer

			c1 = CType(New ElectricalConsumer(False, "Doors", "Doors per Door", 3.0, 0.096339,
											_powernetVoltage, 3, ""), 
						IElectricalConsumer)
			c2 = CType(New ElectricalConsumer(True, "Veh Electronics &Engine", "Controllers,Valves etc",
											25.0, 1.0,
											_powernetVoltage, 1, ""), 
						IElectricalConsumer)
			c3 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Radio City", 2.0, 0.8,
											_powernetVoltage, 1, ""), 
						IElectricalConsumer)
			c4 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Radio Intercity", 5.0,
											0.8, _powernetVoltage,
											0, ""), 
						IElectricalConsumer)
			c5 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Radio/Audio Tourism",
											9.0, 0.8,
											_powernetVoltage, 0, ""), 
						IElectricalConsumer)
			c6 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Fridge", 4.0, 0.5,
											_powernetVoltage, 0, ""), 
						IElectricalConsumer)
			c7 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Kitchen Standard",
											67.0, 0.05, _powernetVoltage,
											0, ""), 
						IElectricalConsumer)
			c8 = CType(New ElectricalConsumer(False, "Vehicle basic equipment",
											"Interior lights City/ Intercity + Doorlights [Should be 1/m]", 1.0, 0.7,
											_powernetVoltage, 12,
											"1 Per metre length of bus"), 
						IElectricalConsumer)
			c9 = CType(New ElectricalConsumer(False, "Vehicle basic equipment",
											"LED Interior lights ceiling city/Intercity + door [Should be 1/m]", 0.6, 0.7,
											_powernetVoltage, 0,
											"1 Per metre length of bus"), 
						IElectricalConsumer)
			c10 = CType(New ElectricalConsumer(False, "Vehicle basic equipment", "Interior lights Tourism + reading [1/m]",
												1.1,
												0.7, _powernetVoltage, 0, "1 Per metre length of bus"), 
						IElectricalConsumer)
			c11 = CType(New ElectricalConsumer(False, "Vehicle basic equipment",
												"LED Interior lights ceiling Tourism + LED reading [Should be 1/m]", 0.66, 0.7,
												_powernetVoltage, 0,
												"1 Per metre length of bus"), 
						IElectricalConsumer)
			c12 = CType(New ElectricalConsumer(False, "Customer Specific Equipment", "External Displays Font/Side/Rear",
												2.65017667844523, 1.0, _powernetVoltage, 4, ""), 
						IElectricalConsumer)
			c13 = CType(New ElectricalConsumer(False, "Customer Specific Equipment",
												"Internal display per unit ( front side rear)", 1.06007067137809, 1.0,
												_powernetVoltage, 1, ""), 
						IElectricalConsumer)
			c14 = CType(New ElectricalConsumer(False, "Customer Specific Equipment",
												"CityBus Ref EBSF Table4 Devices ITS No Displays", 9.3, 1.0, _powernetVoltage, 1,
												""), 
						IElectricalConsumer)
			c15 = CType(New ElectricalConsumer(False, "Lights", "Exterior Lights BULB", 7.4, 1.0,
												_powernetVoltage, 1, ""), 
						IElectricalConsumer)
			c16 = CType(New ElectricalConsumer(False, "Lights", "Day running lights LED bonus",
												-0.723, 1.0, _powernetVoltage,
												1, ""), 
						IElectricalConsumer)
			c17 = CType(New ElectricalConsumer(False, "Lights", "Antifog rear lights LED bonus",
												-0.17, 1.0, _powernetVoltage,
												1, ""), 
						IElectricalConsumer)
			c18 = CType(New ElectricalConsumer(False, "Lights", "Position lights LED bonus", -1.2,
												1.0, _powernetVoltage, 1,
												""), 
						IElectricalConsumer)
			c19 = CType(New ElectricalConsumer(False, "Lights", "Direction lights LED bonus", -0.3,
												1.0, _powernetVoltage, 1,
												""), 
						IElectricalConsumer)
			c20 = CType(New ElectricalConsumer(False, "Lights", "Brake Lights LED bonus", -1.2, 1.0,
												_powernetVoltage, 1, ""), 
						IElectricalConsumer)

			items.Add(c1)
			items.Add(c2)
			items.Add(c3)
			items.Add(c4)
			items.Add(c5)
			items.Add(c6)
			items.Add(c7)
			items.Add(c8)
			items.Add(c9)
			items.Add(c10)
			items.Add(c11)
			items.Add(c12)
			items.Add(c13)
			items.Add(c14)
			items.Add(c15)
			items.Add(c16)
			items.Add(c17)
			items.Add(c18)
			items.Add(c19)
			items.Add(c20)

			Return items
		End Function


		'Interface implementation
		Public Property DoorDutyCycleFraction As Double Implements IElectricalConsumerList.DoorDutyCycleFraction

			Get
				Return _doorDutyCycleZeroToOne
			End Get
			Set(value As Double)
				_doorDutyCycleZeroToOne = value
			End Set
		End Property

		Public ReadOnly Property Items As List(Of IElectricalConsumer) Implements IElectricalConsumerList.Items
			Get
				Return _items
			End Get
		End Property

		Public Sub AddConsumer(consumer As IElectricalConsumer) Implements Electrics.IElectricalConsumerList.AddConsumer

			If Not _items.Contains(consumer) Then
				_items.Add(consumer)
			Else

				Throw New ArgumentException("Consumer Already Present in the list")

			End If
		End Sub

		Public Sub RemoveConsumer(consumer As IElectricalConsumer) Implements Electrics.IElectricalConsumerList.RemoveConsumer

			If _items.Contains(consumer) Then

				_items.Remove(consumer)

			Else

				Throw New ArgumentException("Consumer Not In List")

			End If
		End Sub


		Public Function GetTotalAverageDemandAmps(excludeOnBase As Boolean) As Ampere _
			Implements Electrics.IElectricalConsumerList.GetTotalAverageDemandAmps

			Dim Amps As Ampere

			If excludeOnBase Then
				Amps =
					Aggregate item In Items Where item.BaseVehicle = False Into Sum(item.TotalAvgConumptionAmps(DoorDutyCycleFraction))
			Else
				Amps = Aggregate item In Items Into Sum(item.TotalAvgConumptionAmps(DoorDutyCycleFraction))
			End If


			Return Amps
		End Function
	End Class
End Namespace


