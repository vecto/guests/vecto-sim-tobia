﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.


Namespace Electrics
	'Originally was going to hold more than one value type.
	Public Structure AlternatorMapValues
		Public ReadOnly Efficiency As Double

		Public Sub New(ByVal efficiency As Double)
			Me.Efficiency = efficiency
		End Sub
	End Structure
End Namespace

