﻿' Copyright 2017 European Union.
' Licensed under the EUPL (the 'Licence');
'
' * You may not use this work except in compliance with the Licence.
' * You may obtain a copy of the Licence at: http://ec.europa.eu/idabc/eupl
' * Unless required by applicable law or agreed to in writing,
'   software distributed under the Licence is distributed on an "AS IS" basis,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'
' See the LICENSE.txt for the specific language governing permissions and limitations.

Imports VectoAuxiliaries.Pneumatics
Imports VectoAuxiliaries.Electrics
Imports VectoAuxiliaries.Hvac

Namespace Hvac
	Public Interface IHVACUserInputsConfig
		' Property  SteadyStateModel As IHVACSteadyStateModel
		''' <summary>
		''' PathName of the Steady State Model File
		''' </summary>
		''' <value></value>
		''' <returns></returns>
		''' <remarks></remarks>
		Property SSMFilePath As String

		Property BusDatabasePath As String

		Property SSMDisabled As Boolean
	End Interface
End Namespace


