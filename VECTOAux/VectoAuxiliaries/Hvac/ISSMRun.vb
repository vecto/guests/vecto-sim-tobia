﻿
Namespace Hvac


	Public Interface ISSMRun

		ReadOnly Property HVACOperation As Double
		ReadOnly Property TCalc As Double
		ReadOnly Property TemperatureDelta As Double
		ReadOnly Property QWall As Double
		ReadOnly Property WattsPerPass As Double
		ReadOnly Property Solar As Double
		ReadOnly Property TotalW As Double
		ReadOnly Property TotalKW As Double
		ReadOnly Property FuelW As Double
		ReadOnly Property TechListAmendedFuelW As Double

	End Interface




End Namespace


